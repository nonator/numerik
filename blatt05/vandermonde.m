%function V = vandermonde(v)
%Die Routine soll zu einem gegebenen Vektor die Vandermonde-Matrix ausrechnen.

function V = vandermonde(v)
n = length(v);
i = 1;
a = 1;
while (i<=n)
V(i,a) = v(i)^(n-a);
i = i+1;
if (i>n && a<n)
i=1;
a = a+1;
end
end
