function z = direct_forward_solve(L, b)
% Diese Funktion soll das Gleichunssystem Lz=b lösen, wobei L eine linke untereDreiecksmatrix ist.
assert(L(1,end)==0, "keine linke untere Dreiecksmatrix")
n = length(b);
y = zeros(n);

for i=1:n
    for j=1:n
        a(j) = L(i,j)*y(j);
    end
    a = sum(a);
    y(i) = ( b(i) - a ) / L(i,i);
end
for i=1:n
    z(i) = sum(y(i,1));
end
