function [x, e, v] = myNewton(f, df, x0)
%nach Bisektionsverfahren Nullstelle bestimmen
x(1) = x0;
for i=2:51
    m = x(i-1) - f(x(i-1))/df(x(i-1));
    x(i) = m;
    v(i) = f(m);
    e(i) = abs( x(i) - x(i-1) );
    if( f(m) == 0)
        break
    end
    if ( abs( x(i) - x(i-1) ) < 1e-12 )
        break
    end
end
