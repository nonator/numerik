function myNewtonTest()
% Testet mybisect
format long;
f =@(x) cos(2*x)^2 - x.^2;
df =@(x) -2*sin(4*x) - 2*x;
x0 = 0.75;
x00 = 0;
H = figure;
[x, e, v] = mybisect(f, x00, x0);
[x1, e1, v1] = myNewton(f, df, x0);
semilogy(i=1:length(e), e(i=1:length(e)), 'r', i=1:length(e1), e(i=1:length(e1)), 'b');
saveas(H, 'PA8.1.fig');
saveas(H, 'PA8.1.pdf');
