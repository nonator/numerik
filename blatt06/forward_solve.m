function z = forward_solve(LU, b)
n = length(b);
for i=1:n
    for j=1:n
        if(i>j)
            L(i,j) = LU(i,j);
        elseif(i==j)
            L(i,j) = 1;
        else
            L(i,j) = 0;
        end
    end
end
z = direct_forward_solve(L,b);
