function LU = LU_decompose(A)
R = A;
n = length(A);
L = ones(n);
L = zeros(n);
for i=1:n-1
for k=i+1:n
L(k,i) = R(k,i)/R(i,i);
for j=i:n
R(k,j) = R(k,j)-L(k,i)*R(i,j);
end
end
end
LU = L+R;
