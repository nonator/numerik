function x = backward_solve(LU, z)
n = length(z);
for i=1:n
    for j=1:n
        if(i<=j)
            U(i,j) = LU(i,j);
        else
            U(i,j) = 0;
        end
    end
end
x = direct_backward_solve(U,z);
