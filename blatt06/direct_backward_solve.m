function x = direct_backward_solve(U, z)
% Diese Funktion soll das Gleichunssystem Ux=z lösen, wobei U eine rechte obere Dreiecksmatrix ist.
assert(U(end,1)==0, "keine rechte obere Dreiecksmatrix")
n = length(z);
y = zeros(n);

for i=n:-1:1
    for j=n:-1:1
        a(j) = U(i,j)*y(j);
    end
    a = sum(a);
    y(i) = ( z(i) - a ) / U(i,i);
end
for i=n:-1:1
    x(i) = sum(y(i,1));
end
