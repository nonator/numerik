function [x, e, v] = mybisect(f, x00, x0)
%nach Bisektionsverfahren Nullstelle bestimmen
x(1) = x00
for i=2:1000000000
    m = (x00+x0)/2;
    x(i) = m;
    v(i) = f(m);
    e(i) = abs( x(i) - x(i-1) );
    if ( sign( f(m) ) == sign( f(x00) ) )
        x00 = m;
    else
        x0 = m;
    end
    if( f(m) == 0)
        break
    end
    if ( abs( x(i) - x(i-1) ) < 1e-12 )
        break
    end
end
