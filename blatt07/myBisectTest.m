function myBisectTest()
% Testet mybisect
format long;
f =@(x) cos(2*x)^2 - x.^2;
x0 = 0.75;
x00 = 0;
[x, e, v] = mybisect(f, x00, x0);

H = figure;
semilogy(i=2:length(e), e(i=2:length(e)));
saveas(H, 'PA7.1.fig');
saveas(H, 'PA7.1.pdf');
print('Bisektionsverfahren sollte logaritmische Konvergenz aufweisen --> Gerade im Plot. Dies ist der Fall, also stimmt die Konvergenzrate mit der Theorie überein.');
% Bisektionsverfahren sollte logaritmische Konvergenz aufweisen --> Gerade im Plot. Dies ist der Fall, also stimmt die Konvergenzrate mit der Theorie überein.
