function [x, e, v] = mybisect(f, x00, x0)
% Berechnet die Nullstelle von f mit Intervallschachtelungsverfahren (Bisektion)
% Vektor x der iterierten x_k
% Vektor e der Fehler abs(x_k - x_(k-1)) auf der y_Achse
% Vektor v der Funktionswerte von f(x_k)
if(x00<x0)
    l = x00
    r = x0
    else
    r = x00
    l = x0
    end

while(f(l)!=0 && f(r)!=0)
    if((l-r)>0.01)
        m = (l+r)/2
        x.append(m)
        if(sign(f(m))==sign(f(r)))
            r = m
        else
            l = m
        end
        if(f(l)==0)
            l
        else
            r
        end
        mybisect(f,l,r)
    else
        l
    end
end

