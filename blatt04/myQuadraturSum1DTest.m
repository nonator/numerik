function myQuadraturSum1DTest()
f =@(x) ones./(ones+25*x.^2);
a = -1;
b = 1;
%Trapezregel
N = 2;
p = linspace(a,b,N);
w = [1/2 1/2];
T = myQuadraturSum1D(f,w,p,a,b,N);
%Simpsonregel
N = 3;
p = linspace(a,b,N);
w = [1/6 4/6 1/6];
S = myQuadraturSum1D(f,w,p,a,b,N);
%Milneregel
N = 5;
p = linspace(a,b,N);
w = [7/90 32/90 12/90 32/90 7/90];
M = myQuadraturSum1D(f,w,p,a,b,N);

T
S
M

%Plot
x = linspace(a,b,1000);
fig = figure(1);
plot(x, f(x));
saveas(fig, 'myQuadraturSum1DPlot.fig','pdf');
saveas(fig, 'myQuadraturSum1DPlot.pdf');
