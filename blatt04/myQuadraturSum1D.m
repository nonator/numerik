function [v] = myQuadraturSum1D(f, w, p, a, b, N);
% f fcuntioFunktion
% w Quadraturgewichte
% p Quadraturpunkte
% a untere Grenze
% b obere Grenze
% N Anzahl Wiederholungen
for i=1:N-1
v(i) = (p(i+1)-p(i))*myQuadratur1D(f,w,p);
end
v = sum(v);
