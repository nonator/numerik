function [c] = myQuadratur1D(f,w,p);
%f Funktion, w Quadraturgewichte, p Quadraturpunkte
%f = @myQuadratur;
b = max(p);a=min(p);
c = (b-a)*dot(w,f(p));
