function myQuadratur1DTest()
f =@(x) ones./(x.^2);
%Trapezregel
p = [0.5 1];
%w = [(1/2) (1/2)];
w = [0.5 0.5];
T = myQuadratur1D(f,w,p);
%Simpsonregel
p = [0.5 0.75 1];
w = [1/6 4/6 1/6];
S = myQuadratur1D(f,w,p);
%3/8-Regel
p = [1/2 2/3 5/6 1];
w = [1/8 3/8 3/8 1/8];
I = myQuadratur1D(f,w,p);

T
S
I
