function [c] = myNewtonInterpol(x,f)
    n=length(x)-1
    for i=1:1:(n+1)
        for k=i:1:(n)
            c(i) = ((f((i+k))-f(i))/(x(i+k)-x(i)));
        end
    end
end
