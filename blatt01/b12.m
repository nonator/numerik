% Aufgabe 1.2
% Polynomwerte:
p = [1 -7 21 -35 35 -21 7 -1];
% x-Werte:
x = 0.8:5e-5:1.2;
% Polynom:
figure('visible','off')
plot(x, polyval(p,x))
print('b1.pdf')
clf()
%Horner-Schema:
p7 = @(x)((((((x.-7).*x.+21).*x.-35).*x.+35).*x.-21).*x.+7).*x.-1;
figure('visible','off')
plot(x, polyval(p7,x))
ylim([-1.5e-5, 1.5e-5])
print('b12.pdf')
clf()
