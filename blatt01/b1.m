% Aufgabe 1.1
format long
% zum Vergleich
e^(-5.5)
% Summenfunktion
function a = summe(x,n)
    a = 0
    for i=0:1:n
        a = a+((x^i)/factorial(i));
    end
end
% a)
x=-5.5
for n=3:3:30
    summe(x,n)
end
% b)
x=5.5
for n=3:3:30
    1/summe(x,n)
end
% c)
x=-0.5
for n=3:3:30
    summe(x,n)^11
end
% Die Werte sind verschieden, da der Computer Zwischenergebnisse speihern muss, die er nicht ganz darstellen kann.
