x = -1:1e-3:1;
f=@(x) 1/(1+25*x.^2) %Runge-Funktion
no = [7 12 17] %n Werte für Auswertung
figure('visible','off')
%äquidistante Knoten
for j=1:1:length(no)
n = no(j)
p = 1:n;
for i=0:1:n
    a=@(i) (-1+(2*i)/n);
    aa=polyval(a,p);
end
%Tschebyscheff-Knoten
for i=1:1:n
    t =@(i) cos(((2*i+1)*pi)/(2*n+2));
    tt=polyval(t,p);
end

plot(x, polyval(f, x), 'g')
plot(aa, polyval(f, aa), 'r*')
plot(tt, polyval(f, tt), 'b*')
end
xlim([-1,1])
ylim([-10,10])
print('PA2-1-N7.fig')
print('PA2-1-N7.pdf')
